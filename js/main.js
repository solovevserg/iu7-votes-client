// const BACKEND_ORIGIN = 'http://localhost:3000';
const BACKEND_ORIGIN = 'http://sdal.pw:7200';

async function fetchJson(path, method = 'GET', body) {
    const url = `${BACKEND_ORIGIN}${path}`;
    return await fetch(url, {
        mode: 'cors',
        method,
        body,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => response.json());
}

const api = {
    async getVote(deviceId) {
        const vote = await fetchJson(`/api/votes/${deviceId}`);
        return vote;
    },

    async getAllVotes() {
        const votes = await fetchJson(`/api/votes/`);
        return votes;
    },

    async postVote(vote) {
        const body = JSON.stringify(vote);
        console.log(body);
        return await fetchJson('/api/votes', 'POST', body);
    },
}

const templates = {
    studentsList(votes) {
        return votes.map(vote => `<b>${vote.fullName}</b> ${vote.group}`).join(', ');
    },
    languageLi(lang) {
        return `<li>
                <div>
                    <div style="font-size: ${20 + lang.percent * 170}px;">${lang.name}</div>
                    <div style="width: ${lang.percent * 100}%;" class="percent">
                        <span>
                            <b>${(lang.percent * 100).toFixed(1)}%</b>
                        </span>
                    </div>
                    <span>
                        ${this.studentsList(lang.votes)}
                    </span>
                </div>
            </li>`
    },
    languageLis(langs) {
        return langs.map(lang => templates.languageLi(lang)).join('')
    }
}

const elements = {
    formView: document.getElementById('form-view'),
    resultsView: document.getElementById('results-view'),
    langInput: document.querySelector('input[name="language"]'),
    langOptions: document.querySelectorAll('.language-option'),
    langsOl: document.getElementById('langs-list'),
    submitButton: document.getElementById('submit'),
    form: document.getElementById('vote-form')
}

function getDeviceId() {
    const key = 'deviceId'
    if (!localStorage) {
        throw Error('Sorry, you are not supported');
    }
    let deviceId = localStorage.getItem(key);
    if (!deviceId) {
        deviceId = `${new Date().getTime()}-${Math.random()}`;
        deviceId = localStorage.setItem(key, deviceId);
    }
    return deviceId;
}


function groupByLanguage(votes) {
    const groups = _.groupBy(votes, 'language');
    const metrics = Object.entries(groups).map(([name, langVotes]) => ({
        name,
        votes: langVotes,
        percent: langVotes.length / votes.length
    }));
    return _.orderBy(metrics, 'percent', 'desc');
}

async function handleComplete() {

    const formData = new FormData(elements.form);
    const formObject = Object.fromEntries(formData);
    formObject.deviceId = getDeviceId();
    formObject.time = new Date().getTime();
    console.log(formObject)
    const vote = await api.postVote(formObject)
    if (vote) {
        loadResultView();
    }
    return vote;
}

function addEventListeners() {
    elements.langOptions.forEach(option => {
        option.addEventListener('click', event => {
            elements.langInput.value = event.target.textContent;
        })
    });
    elements.submitButton.addEventListener('click', handleComplete);
}

function loadFormView() {
    elements.formView.style.display = 'block'
    elements.resultsView.style.display = 'none'
    addEventListeners();
}

async function loadResultView() {
    elements.resultsView.style.display = 'block'
    elements.formView.style.display = 'none'
    async function load() {
        const votes = await api.getAllVotes();
        const langs = groupByLanguage(votes);
        const langsListHTML = templates.languageLis(langs);
        elements.langsOl.innerHTML = langsListHTML;
    }
    load();
    setInterval(load, 2500);
}

async function main() {
    const deviceId = getDeviceId();
    const vote = await api.getVote(deviceId);
    if (vote) {
        await loadResultView();
    } else {
        await loadFormView();
    }
}

main();